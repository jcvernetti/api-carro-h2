package br.com.carro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestCarroH2Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestCarroH2Application.class, args);
	}

}
